#!/bin/bash

find . -name "*~" | xargs rm -f
find . -name "*.pyc" | xargs rm -f
