#!/usr/bin/env python

import sys, os

from dolfin import *

parameters["std_out_all_processes"] = False;

# Test for PETSc or Epetra
if not has_linear_algebra_backend("PETSc") and not has_linear_algebra_backend("Epetra"):
    info("DOLFIN has not been configured with Trilinos or PETSc. Exiting.")
    sys.exit(1)

if not has_krylov_solver_preconditioner("hypre_amg"):
    info("Sorry, this demo is only available when DOLFIN is compiled with AMG "
	 "preconditioner, Hypre or ML.");
    sys.exit(1)

try:
    nx = int(sys.argv[1])
    ny = int(sys.argv[2])
    nz = int(sys.argv[3])
except:
    info("Usage: %s nx ny nz" % sys.argv[0])
    sys.exit(1)

# Create or load mesh from file if available
mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (nx, ny, nz)
if os.path.isfile(mesh_filename):
    mesh = Mesh(mesh_filename)
else:
    mesh = UnitCubeMesh(nx, ny, nz)
    XDMFFile(mesh_filename) << mesh

# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)
W = V * Q

# Boundaries
def right(x, on_boundary): return x[0] > (1.0 - DOLFIN_EPS)
def left(x, on_boundary): return x[0] < DOLFIN_EPS
def top_bottom(x, on_boundary):
    return x[1] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS

# No-slip boundary condition for velocity
noslip = Constant((0.0, 0.0, 0.0))
bc0 = DirichletBC(W.sub(0), noslip, top_bottom)

# Inflow boundary condition for velocity
inflow = Expression(("-sin(x[1]*pi)", "0.0", "0.0"))
bc1 = DirichletBC(W.sub(0), inflow, right)

# Boundary condition for pressure at outflow
zero = Constant(0)
bc2 = DirichletBC(W.sub(1), zero, left)

# Collect boundary conditions
bcs = [bc0, bc1, bc2]

# Define variational problem
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
f = Constant((0.0, 0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

# Form for use in constructing preconditioner matrix
b = inner(grad(u), grad(v))*dx + p*q*dx

U = Function(W)

# Create Krylov solver and AMG preconditioner
solver = KrylovSolver("tfqmr", "hypre_amg")

# Assemble system
MPI.barrier()
t = time()
A, bb = assemble_system(a, L, bcs)
MPI.barrier()
assemble_system_time = time() - t

# Assemble preconditioner system
MPI.barrier()
t = time()
P, btmp = assemble_system(b, L, bcs)
# Associate operator (A) and preconditioner matrix (P)
solver.set_operators(A, P)
MPI.barrier()
assemble_preconditioner_time = time() - t

# Solve
MPI.barrier()
t = time()
solver.solve(U.vector(), bb)
MPI.barrier()
solve_time = time() - t

info("Assemble system time: %.5g" % assemble_system_time)
info("Assemble preconditioner time: %.5g" % assemble_preconditioner_time)
info("Solve time: %.5g" % solve_time)
info("Mesh size: %s x %s x %s (unit cube)" % (nx, ny, nz))
info("Size of matrix A: %s x %s" % (A.size(0), A.size(1)))
info("Number of processes: %s" % MPI.num_processes())
