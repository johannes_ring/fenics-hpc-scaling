#!/usr/bin/env python

import sys, os

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    nx = int(sys.argv[1])
    ny = int(sys.argv[2])
    nz = int(sys.argv[3])
except:
    info("Usage: %s nx ny nz" % sys.argv[0])
    sys.exit(1)

# Create or load mesh from file if available
mesh = UnitCubeMesh(nx, ny, nz)
#mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (nx, ny, nz)
#if os.path.isfile(mesh_filename):
#    mesh = Mesh(mesh_filename)
#else:
#    mesh = UnitCubeMesh(nx, ny, nz)
#    XDMFFile(mesh_filename) << mesh

# Define function space
V = VectorFunctionSpace(mesh, "CG", 1)

# Homogenius Dirichlet boundary condition for the rigged facet
def left(x, on_boundary):
    return x[0] < 0.0 + DOLFIN_EPS and on_boundary

def right(x, on_boundary):
    return x[0] > 1.0 - DOLFIN_EPS and on_boundary

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Constant((0.0, 0.0, 0.0))

E  = 10.0
nu = 0.3

mu    = E / (2.0*(1.0 + nu))
lmbda = E*nu / ((1.0 + nu)*(1.0 - 2.0*nu))

def sigma(v):
    return 2.0*mu*sym(grad(v)) + lmbda*tr(sym(grad(v)))*Identity(v.cell().d)

a = inner(sigma(u), grad(v))*dx
L = inner(f, v)*dx

# Set up boundary condition at left end
c = Constant((0.0, 0.0, 0.0))
bcl = DirichletBC(V, c, left)

# Set up boundary condition at right end
r = Constant((0.0,-0.1,-0.1))
bcr = DirichletBC(V, r, right)

# Set up boundary conditions
bcs = [bcl, bcr]

# Compute solution
u = Function(V)

MPI.barrier()
t = time()
A, L2 = assemble_system(a, L, bcs)
MPI.barrier()
assemble_time = time() - t

MPI.barrier()
t = time()
it = solve(A, u.vector(), L2, "gmres", "hypre_amg")
MPI.barrier()
solve_time = time() - t

info("Assemble time: %.5g" % assemble_time)
info("Solve time: %.5g" % solve_time)
info("Number of iterations %d" % it)
info("Total solve time: %.5g" % (assemble_time + solve_time))
info("Mesh size: %s x %s x %s (unit cube)" % (nx, ny, nz))
info("Size of matrix A: %s x %s" % (A.size(0), A.size(1)))
info("Number of processes: %s" % MPI.num_processes())
