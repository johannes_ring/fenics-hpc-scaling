#!/usr/bin/env python

import sys, os

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    nx = int(sys.argv[1])
    ny = int(sys.argv[2])
    nz = int(sys.argv[3])
except:
    info("Usage: %s nx ny nz" % sys.argv[0])
    sys.exit(1)

# Create or load mesh from file if available
mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (nx, ny, nz)
if os.path.isfile(mesh_filename):
    mesh = Mesh(mesh_filename)
else:
    mesh = UnitCubeMesh(nx, ny, nz)
    XDMFFile(mesh_filename) << mesh

# Define function space
V = FunctionSpace(mesh, "Lagrange", 1)

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

# Define variational problem
u = TrialFunction(V)
v = TestFunction(V)
f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)")
g = Expression("sin(5*x[0])")
a = inner(grad(u), grad(v))*dx
L = f*v*dx + g*v*ds
u1 = Function(V)

# Assemble (symmetric system)
MPI.barrier()
t = time()
A, b = assemble_system(a,inner(f,v)*dx + inner(g,v)*ds, bc)
MPI.barrier()
assemble_time = time() - t

#"gmres", "ilu" #tentative
#"gmres", "hypre_amg" #pressure

MPI.barrier()
t = time()
it = solve(A, u1.vector(), b, "cg", "hypre_amg")
MPI.barrier()
solve_time = time() - t

info("Assemble time: %.5g" % assemble_time)
info("Solve time: %.5g" % solve_time)
info("Number of iterations %d" % it)
info("Total solve time: %.5g" % (assemble_time + solve_time))
info("Mesh size: %s x %s x %s (unit cube)" % (nx, ny, nz))
info("Size of matrix A: %s x %s" % (A.size(0), A.size(1)))
info("Number of processes: %s" % MPI.num_processes())
