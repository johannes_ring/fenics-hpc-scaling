#!/usr/bin/env python

import os, sys

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    size = int(sys.argv[1])
    num_reps = int(sys.argv[2])
except:
    info("Usage: %s size repetitions" % sys.argv[0])
    sys.exit(1)

info("Iteration over entities of unit cube of size %d x %d x %d (%d repetitions)" % \
     (size, size, size, num_reps))

mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (size, size, size)
if os.path.isfile(mesh_filename):
    mesh = Mesh(mesh_filename)
else:
    mesh = UnitCubeMesh(size, size, size)
    XDMFFile(mesh_filename) << mesh

sum = 0
MPI.barrier()
t = time()
for i in range(0, num_reps):
    for c in cells(mesh):
        for v in vertices(c):
            sum += v.index()
MPI.barrier()
iteration_time = time() - t

info("Total iteration time: %.5g" % iteration_time)
info("Unit cube dimensions: %s x %s x %s" % (size, size, size))
info("Number of repetitions: %s" % num_reps)
info("Number of processes: %s" % MPI.num_processes())
