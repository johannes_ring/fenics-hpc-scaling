#!/usr/bin/env python

import os, sys

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    size = int(sys.argv[1])
    num_reps = int(sys.argv[2])
except:
    info("Usage: %s size repetitions" % sys.argv[0])
    sys.exit(1)

info("Uniform refinement of unit cube of size %d x %d x %d (%d refinements)" %
     (size, size, size, num_reps))

mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (size, size, size)
if os.path.isfile(mesh_filename):
    mesh = Mesh(mesh_filename)
else:
    mesh = UnitCubeMesh(size, size, size)
    XDMFFile(mesh_filename) << mesh

MPI.barrier()
t = time()
for i in range(num_reps):
    mesh = refine(mesh);
    info("Refined mesh: %s" % mesh)
MPI.barrier()
refine_time = time() - t

info("Total refinement time: %.5g" % refine_time)
info("Mesh size: %s x %s x %s (unit cube)" % (size, size, size))
info("Number of repetitions: %s" % num_reps)
info("Number of processes: %s" % MPI.num_processes())
