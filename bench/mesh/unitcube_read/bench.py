#!/usr/bin/env python

import sys, os

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    size = int(sys.argv[1])
    num_reps = int(sys.argv[2])
except:
    info("Usage: %s size repetitions" % sys.argv[0])
    sys.exit(1)

mesh_filename = "unitcube_mesh_%sx%sx%s.xdmf" % (size, size, size)
if not os.path.isfile(mesh_filename):
    mesh = UnitCubeMesh(size, size, size)
    info("Created unit cube: %s" % mesh)
    XDMFFile(mesh_filename) << mesh
    info("Wrote unit cube mesh to XDMF file.")

info("Reading unit cube of size %d x %d x %d (%d repetitions)" % \
     (size, size, size, num_reps))

MPI.barrier()
t = time()
for i in range(num_reps):
    mesh2 = Mesh(mesh_filename)
    info("Read unit cube mesh from XDMF file.")
MPI.barrier()
read_time = time() - t

info("Total read time: %.5g" % read_time)
info("Unit cube dimensions: %s x %s x %s" % (size, size, size))
info("Number of repetitions: %s" % num_reps)
info("Number of processes: %s" % MPI.num_processes())
