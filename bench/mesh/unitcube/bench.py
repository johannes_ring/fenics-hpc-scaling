#!/usr/bin/env python

import sys

from dolfin import *

parameters["std_out_all_processes"] = False;

try:
    size = int(sys.argv[1])
    num_reps = int(sys.argv[2])
except:
    info("Usage: %s size repetitions" % sys.argv[0])
    sys.exit(1)

info("Creating unit cube of size %d x %d x %d (%d repetitions)" % \
     (size, size, size, num_reps))

MPI.barrier()
t = time()
for i in range(num_reps):
    mesh = UnitCubeMesh(size, size, size)
    info("Created unit cube: %s" % mesh)
MPI.barrier()
create_time = time() - t

info("Total creation time: %.5g" % create_time)
info("Mesh size: %s x %s x %s (unit cube)" % (size, size, size))
info("Number of repetitions: %s" % num_reps)
info("Number of processes: %s" % MPI.num_processes())
