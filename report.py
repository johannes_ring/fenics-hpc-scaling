#!/usr/bin/env python

import sys, os, re

#from scitools.std import *
from numpy import *

def get_logdata(logfiles):
    logdata = {}
    logdatalist = []

    for logfile in logfiles:
        logfilename = os.path.split(logfile)[-1]
        d = {}
        d["ncores"] = None

        pattern = "(.*)_(.*)_(\d+)reps_(\d+)nodes_run(\d+)\..*"
        match = re.search(pattern, logfilename)
        if match:
            bench_name = match.group(1)
            d["bench_name"] = match.group(1)
            d["mesh_size"] = match.group(2)
            d["num_reps"] = match.group(3)
            d["ncores"] = int(match.group(4))
            d["run"] = int(match.group(5))
        else:
            pattern = "(.*)_(.*)_(\d+)nodes_run(\d+)\..*"
            match = re.search(pattern, logfilename)
            if match:
                bench_name = match.group(1)
                d["bench_name"] = match.group(1)
                d["mesh_size"] = match.group(2)
                d["ncores"] = int(match.group(3))
                d["run"] = int(match.group(4))
            else:
                print "Warning: %s not on correct format. Skippping file..." % logfile
                continue

        bench = logfile.strip()
        lines = open(logfile).readlines()
        for line in lines:
            # Find number of processes
            pattern = "Number of processes: (.*)"
            match = re.search(pattern, line)
            if match:
                d["ncores"] = int(match.group(1))

            # Find assemble time
            pattern = "Assemble time: (.*)"
            match = re.search(pattern, line)
            if match:
                d["assemble_time"] = float(match.group(1))

            # Find assemble preconditioner time
            pattern = "Assemble preconditioner time: (.*)"
            match = re.search(pattern, line)
            if match:
                d["assemble_preconditioner_time"] = float(match.group(1))

            # Find assemble system time
            pattern = "Assemble system time: (.*)"
            match = re.search(pattern, line)
            if match:
                d["assemble_system_time"] = float(match.group(1))

            # Find solve time
            pattern = "Solve time: (.*)"
            match = re.search(pattern, line)
            if match:
                d["solve_time"] = float(match.group(1))

            # Find total time
            pattern = "Total (.*) time: (.*)"
            match = re.search(pattern, line)
            if match:
                d["total_time"] = float(match.group(2))

            ## Find mesh size
            #pattern = "Mesh size: (.*)"
            #match = re.search(pattern, line)
            #if match:
            #    d["mesh_size"] = match.group(1)

            # Find number of repetitions
            pattern = "Number of repetitions: (.*)"
            match = re.search(pattern, line)
            if match:
                d["num_reps"] = match.group(1)

        if d["ncores"] is not None:
            logdatalist.append(d)
        else:
            print "Warning: %s does not contain information about number of cores." % logfile

    # Sort log data list on number of cores
    logdatalist = sorted(logdatalist, key=lambda k: k['ncores'])

    for d in logdatalist:
        bname = d["bench_name"] + "_" + d["mesh_size"]
        if not bname in logdata:
            logdata[bname] = {'ncores': [d['ncores']],}
            if d.get('assemble_time', None) is not None:
                logdata[bname]['assemble_time'] = [d['assemble_time']]
            if d.get('assemble_system_time', None) is not None:
                logdata[bname]['assemble_system_time'] = [d['assemble_system_time']]
            if d.get('assemble_preconditioner_time', None) is not None:
                logdata[bname]['assemble_preconditioner_time'] = [d['assemble_preconditioner_time']]
            if d.get('solve_time', None) is not None:
                logdata[bname]['solve_time'] = [d['solve_time']]
            if d.get('total_time', None) is not None:
                logdata[bname]['total_time'] = [d['total_time']]
        else:
            if d['ncores'] not in logdata[bname]['ncores']:
                logdata[bname]['ncores'].append(d['ncores'])
                if logdata[bname].has_key('assemble_time'):
                    logdata[bname]['assemble_time'].append(d.get('assemble_time', nan))
                if logdata[bname].has_key('assemble_system_time'):
                    logdata[bname]['assemble_system_time'].append(d.get('assemble_system_time', nan))
                if logdata[bname].has_key('assemble_preconditioner_time'):
                    logdata[bname]['assemble_preconditioner_time'].append(d.get('assemble_preconditioner_time', nan))
                if logdata[bname].has_key('solve_time'):
                    logdata[bname]['solve_time'].append(d.get('solve_time', nan))
                if logdata[bname].has_key('total_time'):
                    logdata[bname]['total_time'].append(d.get('total_time', nan))
            else:
                i = logdata[bname]['ncores'].index(d['ncores'])
                if logdata[bname].has_key('assemble_time'):
                    logdata[bname]['assemble_time'][i] = min(logdata[bname]['assemble_time'][i], d.get('assemble_time', None))
                if logdata[bname].has_key('assemble_system_time'):
                    logdata[bname]['assemble_system_time'][i] = min(logdata[bname]['assemble_system_time'][i], d.get('assemble_system_time', None))
                if logdata[bname].has_key('assemble_preconditioner_time'):
                    logdata[bname]['assemble_preconditioner_time'][i] = min(logdata[bname]['assemble_preconditioner_time'][i], d.get('assemble_preconditioner_time', None))
                if logdata[bname].has_key('solve_time'):
                    logdata[bname]['solve_time'][i] = min(logdata[bname]['solve_time'][i], d.get('solve_time', nan))
                if logdata[bname].has_key('total_time'):
                    logdata[bname]['total_time'][i] = min(logdata[bname]['total_time'][i], d.get('total_time', nan))

    return logdata

def speedup(t):
    """Return the speedup S(N) = T(1)/T(N)"""
    return t[0]/asarray(t)

def efficiency(n, t):
    """Return the efficiency E(N) = S(N)/N = T(1)/(N*T(N))"""
    return t[0]/(n*asarray(t))

def strong_scaling(n, t):
    """Return the strong scaling T(1) / (N * T(N) ) * 100%"""
    return t[0]/(n*asarray(t))*100

def plot_speedup(ncores, cputimes, legend):
    y = speedup(cputimes)

    plot(ncores, ncores, '--',
         xlim=[0, ncores[-1]],
         title="Parallel Speedup for DOLFIN",
         xlabel="Number of processing elements",
         ylabel="Speedup",
         legend="perfect speedup",
         axis="square",
         hold="on",
         )

    plot(ncores, y, 's-', legend=legend)

    g = get_backend()
    if backend == "gnuplot":
        g('set xtics (%s)' % ','.join([str(n) for n in ncores]))
        g.replot()
    elif backend == "matplotlib":
        g.gca().set_xticks(ncores)
        g.draw()

def test_efficiency(ncores, cputimes, legend=""):
    y = efficiency(ncores, cputimes)
            
    plot(ncores, y, 's-',
         xlim=[0, ncores[-1]],
         ylim=[0, 1],
         title="Parallel Efficiency for DOLFIN",
         xlabel="Number of processing elements",
         ylabel="Efficiency",
         legend=legend,
         axis="square",
         )
         
    g = get_backend()
    if backend == "gnuplot":
        g('set xtics (%s)' % ','.join([str(n) for n in ncores]))
        g.replot()
    elif backend == "matplotlib":
        g.gca().set_xticks(ncores)
        g.draw()
        
def plot_efficiency(ncores, cputimes, legend=""):
    y = efficiency(ncores, cputimes)*100

    plot(ncores, y, 's-',
         xlim=[0, ncores[-1]],
         #ylim=[0, 100],                                                                                 
         title="Parallel Scaling: Strong Scaling for DOLFIN",
         xlabel="Number of processing elements",
         ylabel="Parallel Efficiency (% of linear scaling)",
         legend=legend,
         axis="square",
         )

    #g = get_backend()
    #if backend == "gnuplot":
    #    g('set xtics (%s)' % ','.join([str(n) for n in ncores]))
    #    g.replot()
    #elif backend == "matplotlib":
    #    g.gca().set_xticks(ncores)
    #    g.draw()

def generate_report(logdata):
    for bname in logdata:
        table_string = "===== %s =====\n\n" % bname
        table_string += "=== CPU time ===\n\n"
        table_string += "|----------------------------|\n| #cpu |"
        table_data = [logdata[bname]['ncores']]
        if logdata[bname].has_key('assemble_time'):
            table_string += " assemble | "
            table_data.append(logdata[bname]['assemble_time'])
        if logdata[bname].has_key('assemble_system_time'):
            table_string += " assemble system | "
            table_data.append(logdata[bname]['assemble_system_time'])
        if logdata[bname].has_key('assemble_preconditioner_time'):
            table_string += " assemble prec | "
            table_data.append(logdata[bname]['assemble_preconditioner_time'])
        if logdata[bname].has_key('solve_time'):
            table_string += " solve | "
            table_data.append(logdata[bname]['solve_time'])
        if logdata[bname].has_key('total_time'):
            table_string += " total | "
            table_data.append(logdata[bname]['total_time'])
        table_string += "\n"
        table_string += "|--" + "".join(["--r--" for i in range(len(table_data))]) + "--|\n"
        for t in zip(*table_data):
            table_string += "| " + ' | '.join([str(s) for s in t]) + " |\n"
        table_string += "|---------------------|\n\n"

        table_string += "=== Speedup ===\n\n"
        table_string += "|----------------------------|\n| #cpu |"
        table_data = [logdata[bname]['ncores']]
        if logdata[bname].has_key('assemble_time'):
            table_string += " assemble | "
            table_data.append(speedup(logdata[bname]['assemble_time']))
        if logdata[bname].has_key('assemble_system_time'):
            table_string += " assemble system | "
            table_data.append(speedup(logdata[bname]['assemble_system_time']))
        if logdata[bname].has_key('assemble_preconditioner_time'):
            table_string += " assemble prec | "
            table_data.append(speedup(logdata[bname]['assemble_preconditioner_time']))
        if logdata[bname].has_key('solve_time'):
            table_string += " solve | "
            table_data.append(speedup(logdata[bname]['solve_time']))
        if logdata[bname].has_key('total_time'):
            table_string += " total | "
            table_data.append(speedup(logdata[bname]['total_time']))
        table_string += "\n"
        table_string += "|--" + "".join(["--r--" for i in range(len(table_data))]) + "--|\n"
        for t in zip(*table_data):
            table_string += "| " + ' | '.join([str(s) for s in t]) + " |\n"
        table_string += "|---------------------|\n\n"

        table_string += "=== Strong scaling ===\n\n"
        table_string += "|----------------------------|\n| #cpu |"
        table_data = [logdata[bname]['ncores']]
        if logdata[bname].has_key('assemble_time'):
            table_string += " assemble | "
            table_data.append(efficiency(logdata[bname]['ncores'],
                                         logdata[bname]['assemble_time'])*100)
        if logdata[bname].has_key('assemble_system_time'):
            table_string += " assemble system | "
            table_data.append(efficiency(logdata[bname]['ncores'],
                                         logdata[bname]['assemble_system_time'])*100)
        if logdata[bname].has_key('assemble_preconditioner_time'):
            table_string += " assemble prec | "
            table_data.append(efficiency(logdata[bname]['ncores'],
                                         logdata[bname]['assemble_preconditioner_time'])*100)
        if logdata[bname].has_key('solve_time'):
            table_string += " solve | "
            table_data.append(efficiency(logdata[bname]['ncores'],
                                         logdata[bname]['solve_time'])*100)
        if logdata[bname].has_key('total_time'):
            table_string += " total | "
            table_data.append(efficiency(logdata[bname]['ncores'],
                                         logdata[bname]['total_time'])*100)
        table_string += "\n"
        table_string += "|--" + "".join(["--r--" for i in range(len(table_data))]) + "--|\n"
        for t in zip(*table_data):
            table_string += "| " + ' | '.join([str(s) for s in t]) + " |\n"
        table_string += "|---------------------|\n\n"
        # FIXME: Bugfix for doconce
        table_string += "\n\nDoconce needs a fix!\n\n"
        print table_string

        #doconce_table = ""
        ##plot_speedup(logdata[bname]['ncores'], logdata[bname]['assemble_time'], legend="assemble")
        #plot_speedup(logdata[bname]['ncores'], logdata[bname]['solve_time'], legend="solve")
        #figure()
        ##plot_efficiency(logdata[bname]['ncores'], logdata[bname]['assemble_time'], legend="assemble")
        #test_efficiency(logdata[bname]['ncores'], logdata[bname]['solve_time'], legend="solve")
        ##for i in range(len(logdata[bname]['ncores'])):
        ##    print logdata[bname]['ncores'][i], logdata[bname]['total_time'][i]
    #raw_input('press enter')

if __name__ == "__main__":
    if len(sys.argv) >= 2:
        logfiles = sys.argv[1:]
    else:
        print "usage: %s logfiles" % sys.argv[0]
        sys.exit(1)

    logdata = get_logdata(logfiles)
    generate_report(logdata)
