#!/usr/bin/env python

from dolfin_utils.pjobs import submit
from config.abel import config

def test_mesh_refinement(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1
    kwargs["mem"] = "16G"

    size = 8
    num_reps = 5
    cmd = "mpirun python bench.py %d %d" % (size, num_reps)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/mesh/refinement/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "mesh_refinement_%dx%dx%d_%dreps_%dnodes_run%d" % \
                (size, size, size, num_reps, nodes, num_runs)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = kwargs["setup"] % setup_args
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            submit(jobs, **kwargs)

def test_mesh_unitcube_read(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1
    kwargs["mem"] = "16G"

    size = 160
    num_reps = 10
    cmd = "mpirun python bench.py %d %d" % (size, num_reps)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/mesh/unitcube_read/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "mesh_unitcube_read_%dx%dx%d_%dreps_%dnodes_run%d" % \
                (size, size, size, num_reps, nodes, run)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            submit(jobs, **kwargs)

def test_fem_poisson(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1

    size = 160
    cmd = "mpirun python bench.py %d %d %d" % (size, size, size)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/fem/poisson/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "fem_poisson_%dx%dx%d_%dnodes_run%d" % \
                (size, size, size, nodes, run)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            if nodes <= 16:
                kwargs["mem"] = "32G"
            else:
                kwargs["mem"] = "16G"
            submit(jobs, **kwargs)

def test_fem_linearelasticity(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1

    size = 128
    cmd = "mpirun python bench.py %d %d %d" % (size, size, size)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/fem/linearelasticity/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "fem_linearelasticity_%dx%dx%d_%dnodes_run%d" % \
                (size, size, size, nodes, run)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            if nodes <= 16:
                kwargs["mem"] = "32G"
            else:
                kwargs["mem"] = "16G"
            submit(jobs, **kwargs)

def test_fem_stokes(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1

    size = 48
    cmd = "mpirun python bench.py %d %d %d" % (size, size, size)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/fem/stokes/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "fem_stokes_%dx%dx%d_%dnodes_run%d" % \
                (size, size, size, nodes, run)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            if nodes <= 16:
                kwargs["mem"] = "32G"
            else:
                kwargs["mem"] = "16G"
            submit(jobs, **kwargs)

def test_petsc(num_runs=1, dryrun=False):
    kwargs = config.copy()    
    kwargs["dryrun"] = dryrun
    kwargs["walltime"] = 1

    size = 324
    cmd = "mpirun ./ex34 -da_grid_x %d -da_grid_y %d -da_grid_z %d -pc_type mg -pc_mg_type full -ksp_type fgmres -ksp_monitor_short -pc_mg_levels 3 -mg_coarse_pc_factor_shift_type nonzero -ksp_view" % (size, size, size)
    jobs = [cmd]

    # FIXME: this is Abel specific
    setup_args = {"inputfiles": "$SUBMITDIR/bench/petsc/*"}
    setup_template = kwargs["setup"]

    for run in range(1, num_runs+1):
        for nodes in [2**i for i in range(10)]:
            jobname = "petsc_%dx%dx%d_%dnodes_run%d" % \
                (size, size, size, nodes, run)
            setup_args["outputfile"] = "output/" + jobname + ".log"
            kwargs["setup"] = setup_template % setup_args
            kwargs["nodes"] = nodes
            kwargs["name"] = jobname
            if nodes <= 16:
                kwargs["mem"] = "32G"
            else:
                kwargs["mem"] = "16G"
            submit(jobs, **kwargs)

if __name__ == "__main__":
    dryrun = True
    #test_mesh_refinement(dryrun=dryrun)
    #test_mesh_unitcube_read(dryrun=dryrun)
    #test_fem_poisson(num_runs=3, dryrun=dryrun)
    #test_fem_linearelasticity(num_runs=3, dryrun=dryrun)
    #test_fem_stokes(num_runs=3, dryrun=dryrun)
    test_petsc(num_runs=3, dryrun=dryrun)
