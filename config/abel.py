# Configuration file for Abel

config = {}

config["backend"] = "slurm"

config["setup"] = """
## Set up job environment
source /cluster/bin/jobsetup
export OMPI_MCA_mpi_warn_on_fork=0
source /usit/abel/u1/johannr/fenics-dev-gcc-2013.08.23.conf
export DOLFIN_NOPLOT=1

## Project:
#SBATCH --account=nn9278k

## Output file
#SBATCH --output=%(outputfile)s

## Set up input and output files:
cp -r %(inputfiles)s $SCRATCH/
#chkfile OutputFile

cd $SCRATCH
"""

config["keep_environment"] = False
